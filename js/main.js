$(function(){

  /* TOGGLE MENU */
  $('.Page__Bars, .Header__Close').click(function(){
    $('.Header').fadeToggle();
  });
  /* TOGGLE MENU */
  
  /* SCROLL DE EXPOSITORES */
  $('.Expositores__Galeria .item img').eq(0).load(function(){
		heightGaleria();
	});

	var img = $('.Expositores__Galeria .item img').eq(0);
	if (img.prop('complete')) {
	  heightGaleria();
  };
  
  function heightGaleria(){
    var heightExpositores = $('.Expositores__Galeria .item').height();
    $('.Expositores__Galeria').css('height',(heightExpositores * 1.4));
  }

  $('.Expositores__GaleriaScroll.Down').click(function(){
    var heightExpositores = $('.Expositores__Galeria .item').height();
    var scrollActual = $('.Expositores__Galeria').scrollTop();
    $('.Expositores__Galeria').animate({ scrollTop: scrollActual + (heightExpositores * 0.4) }, 500);
  });

  $('.Expositores__GaleriaScroll.Up').click(function(){
    var heightExpositores = $('.Expositores__Galeria .item').height();
    var scrollActual = $('.Expositores__Galeria').scrollTop();
    if(scrollActual != 0){
      $('.Expositores__Galeria').animate({ scrollTop: scrollActual - (heightExpositores * 0.4)  }, 500);
    }
  });
  /* SCROLL DE EXPOSITORES */


  /* LIGHTBOX */
  $('.Page').on('click','.Page__LightBox .close',function(){

    $('.Page__Opacity, .Page__LightBox').fadeToggle();
  });
  

  $('.Expositores__Galeria .item_content').click(function(){
    $('.Page__LightBox').html('<div class="close icon-close"></div>');
    $(this).find('img').clone().appendTo('.Page__LightBox');
    $(this).find('h3').clone().appendTo('.Page__LightBox');
    $(this).parent('.item').find('p').clone().appendTo('.Page__LightBox');
    $('.Page__Opacity, .Page__LightBox').fadeToggle();
  });
  /* LIGHTBOX */


  $.validator.setDefaults({
		errorPlacement: function (error, element) {
      if(element.attr("type") == "radio" || element.attr("type") == "checkbox"){

      }else{
        error.insertAfter(element);
      }
		},
		highlight: function (element) {
			$(element).addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).removeClass('has-error');
		}
	});
  /*=============================================>>>>>
  = VALIDACION DE FORMULARIO =
  ===============================================>>>>>*/

  $('#FormDejaInfo').validate({
		rules: {
			nombre:{required:true},
			correo:{required:true,email:true},
		},
		messages: {
			nombre:{required:'Este campo es requerido.'},
			correo:{required:'Este campo es requerido.',email:'Porfavor ingrese un email valido.'}
		},
		submitHandler:function(form) {
      $('#FormDejaInfo').ajaxSubmit({
        success : function(data){
          if(data=='ok'){
            $('.Expositores__Form .MensajeFinal, .Expositores__Form .Formulario').fadeToggle();
          }
				}
			});
		}
	});
  /*= End of VALIDACION DE FORMULARIO =*/
  /*=============================================<<<<<*/



  /* ABRIR LIGHT BOX RESERVA STAND */
  $('.Stands__Informate .link').click(function(){
    $('.Page__Opacity, .Page__LightBox').fadeToggle();
  });
  /* ABRIR LIGHT BOX RESERVA STAND */


  /*=============================================>>>>>
  = VALIDACION DE FORMULARIO =
  ===============================================>>>>>*/

  $('#FormReservaStand').validate({
		rules: {
			nombre:{required:true},
      email:{required:true,email:true},
      telefono:{required:true},
      politicas:{required:true},
		},
		messages: {
			nombre:{required:'Este campo es requerido.'},
      email:{required:'Este campo es requerido.',email:'Porfavor ingrese un email valido.'},
      telefono:{required:'Este campo es requerido.'},
		},
		submitHandler:function(form) {
      $('#FormReservaStand').ajaxSubmit({
        success : function(data){
          if(data=='ok'){
            $('.Page__LightBox .Felicitaciones, .Page__LightBox .Formulario').fadeToggle();
          }
				}
			});
		}
	});
  /*= End of VALIDACION DE FORMULARIO =*/
  /*=============================================<<<<<*/

  
  
  $('.Page__Contacto a, .ContactoClose').click(function(event){
    event.preventDefault();
    $('body').animate({ scrollTop: 0 }, 500);
    $('.ContactoForm').fadeToggle();
    map = new GMaps({
      div: '#Mapa',
      lat: -12.043333,
      lng: -77.028333
    });
  });
   /*=============================================>>>>>
  = VALIDACION DE FORMULARIO CONTACTO =
  ===============================================>>>>>*/
  $('#FormContacto').validate({
		rules: {
			nombre:{required:true},
      correo:{required:true,email:true},
      empresa:{required:true},
      telefono:{required:true},
      mensaje:{required:true}
		},
		messages: {
			nombre:{required:'Este campo es requerido.'},
      correo:{required:'Este campo es requerido.',email:'Porfavor ingrese un correo valido.'},
      empresa:{required:'Este campo es requerido.'},
      telefono:{required:'Este campo es requerido.'},
      mensaje:{required:'Este campo es requerido.'},
		},
		submitHandler:function(form) {
      $('#FormContacto').ajaxSubmit({
        success : function(data){
          if(data=='ok'){
            $('.ContactoForm').fadeToggle();
          }
				}
			});
		}
	});
  /*= End of VALIDACION DE FORMULARIO CONTACTO =*/
  /*=============================================<<<<<*/

});